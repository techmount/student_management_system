package com.akhil28_02.sms;

//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//import com.akhil28_02.sms.entity.Student;
//import com.akhil28_02.sms.repository.StudentRepository;

@SpringBootApplication
public class StudentManagementSystemApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(StudentManagementSystemApplication.class, args);
	}
	
//	@Autowired
//	private StudentRepository studentRepository;

	@Override
	public void run(String... args) throws Exception {
//		Student stud1 = new Student("Akhilesh", "Kumar", "akhil28021996@gmail.com");
//		studentRepository.save(stud1);
//		Student stud2 = new Student("Praveen", "Kumar", "praveen98@gmail.com");
//		studentRepository.save(stud2);
//		Student stud3 = new Student("Vasanth", "Kumar", "vasantha97@gmail.com");
//		studentRepository.save(stud3);
	}

}
